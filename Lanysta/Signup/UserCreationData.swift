//
//  UserCreationData.swift
//  Lanysta
//
//  Created by Randy Hattab on 3/22/19.
//  Copyright © 2019 Lanysta. All rights reserved.
//

import Foundation

class UserCreationData {
    var firstName: String
    var lastName: String
    var phoneNumber: String
    var email: String
    var password: String
    var referralCode: String
    
    init?(firstName: String, lastName: String, phoneNumber: String, email: String, password: String, referralCode: String) {
        guard !firstName.isEmpty else {
            print("First name is Empty")
            return nil
        }
        
        guard !lastName.isEmpty else {
            print("Last name is Empty")
            return nil
        }
        
        guard !phoneNumber.isEmpty else {
            print("Phone number is Empty")
            return nil
        }
        
        guard !email.isEmpty else {
            print("Email is Empty")
            return nil
        }
        
        guard !password.isEmpty else {
            print("Password is Empty")
            return nil
        }
        
        guard !referralCode.isEmpty else {
            print("Referral code is Empty")
            return nil
        }
        
        self.firstName = firstName
        self.lastName = lastName
        self.phoneNumber = phoneNumber
        self.email = email
        self.password = password
        self.referralCode = referralCode
    }
}
































































