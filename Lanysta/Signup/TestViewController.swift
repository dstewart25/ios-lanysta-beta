//
//  TestViewController.swift
//  Lanysta
//
//  Created by Daniel Stewart on 3/28/19.
//  Copyright © 2019 Lanysta. All rights reserved.
//

import UIKit

class TestViewController: UIViewController {
    
    let testLabel: UILabel = {
        let label = UILabel()
        label.text = "TEST"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.addSubview(testLabel)
        view.backgroundColor = .white
        
        NSLayoutConstraint.activate([
            testLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            testLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor)
            ])
    }

}
