//
//  SignUpViewController.swift
//  Lanysta
//
//  Created by Daniel Stewart on 3/19/19.
//  Copyright © 2019 Lanysta. All rights reserved.
//

import UIKit
import AWSMobileClient

class NameAndNumberController: UIViewController, UINavigationControllerDelegate, UITextFieldDelegate {
    var sentTo: String?
    var email: String?
    var password: String?
    
    var firstNameField: UITextField!
    var lastNameField: UITextField!
    var emailField: UITextField!
    var phoneNumberField: UITextField!
    var passwordField: UITextField!
    var confirmPasswordField: UITextField!
    var signUpButtonPressed: UIButton!

    
    let orangeBackground: UIView = {
        let view = UIView(frame: .zero)
        
        let lightOrange = UIColor.rgb(red: 248, green: 148, blue: 6).cgColor
        let darkOrange = UIColor.rgb(red: 249, green: 105, blue: 14).cgColor
        
        let layer = CAGradientLayer()
        layer.colors = [lightOrange, darkOrange]
        layer.locations = [0.0, 1.0]
        layer.frame = view.bounds
        view.layer.addSublayer(layer)
        return view
    }()
    
    let appLogo: UILabel = {
        let label = UILabel()
        label.text = "Lanysta"
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 40)
        return label
    }()
    
    let createAnAccountLabel: UILabel = {
        let label = UILabel()
        label.text = "Create An Account"
        label.textColor = UIColor.rgb(red: 17, green: 154, blue: 237)
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()
    
    let loginContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 4
        view.layer.shadowOffset = CGSize(width: 0, height: 3)
        view.layer.shadowRadius = 3
        view.layer.shadowOpacity = 0.5
        return view
    }()
    
    let firstNameTextField: UITextField = {
        let name = UITextField()
        name.placeholder = "First Name"
        name.backgroundColor = UIColor(white: 0, alpha: 0.03)
        name.borderStyle = .roundedRect
        name.font = UIFont.systemFont(ofSize: 14)
        name.textContentType = .givenName
        name.autocapitalizationType = .none
        name.returnKeyType = .next
        
        name.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        
        return name
    }()
    
    let lastNameTextField: UITextField = {
        let name = UITextField()
        name.placeholder = "Last Name"
        name.backgroundColor = UIColor(white: 0, alpha: 0.03)
        name.borderStyle = .roundedRect
        name.font = UIFont.systemFont(ofSize: 14)
        name.textContentType = .familyName
        name.autocapitalizationType = .none
        name.returnKeyType = .next
        
        name.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        
        return name
    }()
    
    let phoneNumberTextField: UITextField = {
        let phoneNumber = UITextField()
        phoneNumber.placeholder = "Phone Number"
        phoneNumber.backgroundColor = UIColor(white: 0, alpha: 0.03)
        phoneNumber.borderStyle = .roundedRect
        phoneNumber.font = UIFont.systemFont(ofSize: 14)
        phoneNumber.keyboardType = .numberPad
        phoneNumber.textContentType = .telephoneNumber
        phoneNumber.autocapitalizationType = .none
        phoneNumber.returnKeyType = .next
        
        phoneNumber.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        
        return phoneNumber
    }()
    
    let nextButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Next", for: .normal)
        button.backgroundColor = UIColor.rgb(red: 149, green: 204, blue: 244)
        button.layer.cornerRadius = 5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(handleNext), for: .touchUpInside)
        button.isEnabled = true
        return button
    }()
    
    @objc func handleNext() {
        print("Next Pressed")
        let emailAndPSController = EmailAndPSController()
        navigationController?.pushViewController(emailAndPSController, animated: true)
    }
    
    @objc func handleTextInputChange() {
        let isFormValid = firstNameTextField.text?.isEmpty == false && lastNameTextField.text?.isEmpty == false && phoneNumberTextField.text?.isEmpty == false
        
        if isFormValid {
            nextButton.isEnabled = true
            nextButton.backgroundColor = UIColor.rgb(red: 17, green: 154, blue: 237)
        } else {
            nextButton.isEnabled = false
            nextButton.backgroundColor = UIColor.rgb(red: 149, green: 204, blue: 244)
        }
    }
    
//    let referralCodeTextField: UITextField = {
//        let code = UITextField()
//        code.placeholder = "Referral Code"
//        code.backgroundColor = UIColor(white: 0, alpha: 0.03)
//        code.borderStyle = .roundedRect
//        code.font = UIFont.systemFont(ofSize: 14)
//        code.returnKeyType = .go
//
//        code.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
//
//        return code
//    }()
    
//    let signUpButton: UIButton = {
//        let button = UIButton(type: .system)
//        button.setTitle("Sign Up", for: .normal)
//        button.backgroundColor = UIColor.rgb(red: 149, green: 204, blue: 244)
//        button.layer.cornerRadius = 5
//        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
//        button.setTitleColor(.white, for: .normal)
//
//        button.addTarget(self, action: #selector(handleSignUp), for: .touchUpInside)
//
//        button.isEnabled = false
//
//        return button
//    }()
    
    @objc func handleSignUp() {
//        print("Sign Up Button Pressed")
//
//        //let imageView = UIImageView(image: UIImage(named: "test"))
//
//        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
//        activityIndicator.center = self.view.center
//        activityIndicator.hidesWhenStopped = true
//        activityIndicator.style = .gray
//        view.addSubview(activityIndicator)
//        activityIndicator.startAnimating()
//        UIApplication.shared.beginIgnoringInteractionEvents()
//
//        guard let firstName = firstNameTextField.text, firstNameTextField.text?.isEmpty == false,
//            let lastName = lastNameTextField.text, lastNameTextField.text?.isEmpty == false,
////            let email = emailTextField.text?.lowercased(), emailTextField.text?.isEmpty == false,
//            let phoneNumber = phoneNumberTextField.text, phoneNumberTextField.text?.isEmpty == false else {
////            let password = passwordTextField.text, passwordTextField.text?.isEmpty == false,
////            let confirmPassword = confirmPasswordTextField.text, confirmPasswordTextField.text?.isEmpty == false,
////            let referralCode = referralCodeTextField.text else {
//
//                activityIndicator.stopAnimating()
//                let alertController = UIAlertController(title: "Missing Required Fields",
//                                                        message: "All fields are required for registration.",
//                                                        preferredStyle: .alert)
//                let okAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
//                alertController.addAction(okAction)
//                self.present(alertController, animated: true, completion:  nil)
//
//                UIApplication.shared.endIgnoringInteractionEvents()
//                return
//        }
        
//        guard password == confirmPassword else {
//            activityIndicator.stopAnimating()
//            let alertController = UIAlertController(title: "Passwords Do Not Match", message: "Please re-enter your password.", preferredStyle: .alert)
//            let okAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
//            alertController.addAction(okAction)
//            self.present(alertController, animated: true, completion:  nil)
//
//            UIApplication.shared.endIgnoringInteractionEvents()
//            return
//        }
    
//        let attributes = [
//            "given_name": firstName,
//            "family_name": lastName,
//            "email": email,
//            "phone_number": "+1" + phoneNumber,
//            "custom:referral_code": referralCode
//        ]
        
//        AWSMobileClient.sharedInstance().signUp(username: email, password: password, userAttributes: attributes) { (signUpResult, error) in
//            if let signUpResult = signUpResult {
//                switch(signUpResult.signUpConfirmationState) {
//                case .confirmed:
//                    print("User is signed up and confirmed.")
//                    DispatchQueue.main.async {
//                        let alertController = UIAlertController(title: "Congratulations", message: "You are now signed up! Check your email for your referral code.", preferredStyle: .alert)
//                        let okAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
//                        alertController.addAction(okAction)
//                        self.present(alertController, animated: true, completion:  nil)
//                    }
//
//                case .unconfirmed:
//                    print("SHOULD NEVER BE HIT")
//                    print("User is not confirmed and needs verification")
//                default:
//                    print("SHOULD NEVER BE HIT")
//                    print("Unexpected case")
//                }
//            } else if let error = error {
//                if let error = error as? AWSMobileClientError {
//                    switch(error) {
//                    case .usernameExists(let message):
//                        print(message)
//                        DispatchQueue.main.async {
//                            let alertController = UIAlertController(title: "Email In Use", message: message, preferredStyle: .alert)
//                            let okAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
//                            alertController.addAction(okAction)
//                            self.present(alertController, animated: true, completion:  nil)
//                        }
//                    case .userLambdaValidation(let message):
//                        print(message)
//                        DispatchQueue.main.async {
//                            let newMessage = String(message.dropFirst(28))
//
//                            let alertController = UIAlertController(title: "Error", message: newMessage, preferredStyle: .alert)
//                            let okAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
//                            alertController.addAction(okAction)
//                            self.present(alertController, animated: true, completion:  nil)
//                        }
//                    case .invalidPassword(let message):
//                        DispatchQueue.main.async {
//                            let alertController = UIAlertController(title: "Invalid Password", message: message, preferredStyle: .alert)
//                            let okAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
//                            alertController.addAction(okAction)
//                            self.present(alertController, animated: true, completion:  nil)
//                        }
//                    default:
//                        print("SHOULD NEVER BE HIT")
//                        print(error)
//                        break
//                    }
//                }
//                print(error)
//                print("\(error.localizedDescription)")
//            }
//
//            DispatchQueue.main.async {
//                activityIndicator.stopAnimating()
//                UIApplication.shared.endIgnoringInteractionEvents()
//            }
//        }
        
        let emailAndPSController = EmailAndPSController()
//        navigationController?.pushViewController(emailAndPSController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setGradientBackground()
        
        view.addSubview(appLogo)
        appLogo.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: view.frame.height/12-25, paddingLeft: 0, paddingBotton: 0, paddingRight: 0, width: view.frame.width, height: 50)
        
        // Watching for touch out of keyboard to remove keyboard
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        
        setupInputFields()
    }
    
    func setGradientBackground() {
        let lightOrange = UIColor.rgb(red: 248, green: 148, blue: 6).cgColor
        let darkOrange = UIColor.rgb(red: 249, green: 105, blue: 14).cgColor
        
        let layer = CAGradientLayer()
        layer.colors = [lightOrange, darkOrange]
        layer.locations = [0.0, 1.0]
        layer.frame = self.view.bounds
        self.view.layer.addSublayer(layer)
    }
    
    fileprivate func setupInputFields() {
        
//        view.addSubview(nextButton)
//        nextButton.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 16, paddingBotton: 0, paddingRight: 16, width: 0, height: 50)
        
        view.addSubview(loginContainerView)
        loginContainerView.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, paddingTop: 20, paddingLeft: 16, paddingBotton: (view.frame.height/2)-80, paddingRight: 16, width: 0, height: 270)
        
        view.addSubview(createAnAccountLabel)
        createAnAccountLabel.anchor(top: loginContainerView.topAnchor, left: loginContainerView.leftAnchor, bottom: nil, right: loginContainerView.rightAnchor, paddingTop: 20, paddingLeft: 0, paddingBotton: 0, paddingRight: 0, width: 0, height: 50)
        
        let nameStackView = UIStackView(arrangedSubviews: [firstNameTextField, lastNameTextField])
        nameStackView.distribution = .fillEqually
        nameStackView.axis = .horizontal
        nameStackView.spacing = 10
        
        let stackView = UIStackView(arrangedSubviews: [nameStackView, phoneNumberTextField, nextButton])
        
        // Makes everything inside of the stackView array become distributed equally
        stackView.distribution = .fillEqually
        // Aligns the contents inside the stackView array to be aligned veritcally
        stackView.axis = .vertical
        // stackviews spacing inbetween elements of the array
        stackView.spacing = 10
        
        view.addSubview(stackView)
        
        // Auto Layout
        stackView.anchor(top: createAnAccountLabel.bottomAnchor, left: loginContainerView.leftAnchor, bottom: loginContainerView.bottomAnchor, right: loginContainerView.rightAnchor, paddingTop: 20, paddingLeft: 20, paddingBotton: 20, paddingRight: 20, width: 0, height: 0)
        
        phoneNumberTextField.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        print("Keyboard has Appeared")
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        print("Keyboard has disappeared")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case firstNameTextField:
            lastNameTextField.becomeFirstResponder()
        case lastNameTextField:
            phoneNumberTextField.becomeFirstResponder()
        default:
            phoneNumberField.resignFirstResponder()
//            let emailAndPSController = EmailAndPSController()
//            navigationController?.pushViewController(emailAndPSController, animated: true)
        }
        return true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}















































