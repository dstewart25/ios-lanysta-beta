//
//  SecondSignUpController.swift
//  Lanysta
//
//  Created by Randy Hattab on 3/22/19.
//  Copyright © 2019 Lanysta. All rights reserved.
//

import UIKit

class SecondSignUpController: UIViewController, UITextFieldDelegate {
    
    let orangeBackground: UIView = {
        let view = UIView(frame: .zero)
        
        let lightOrange = UIColor.rgb(red: 248, green: 148, blue: 6).cgColor
        let darkOrange = UIColor.rgb(red: 249, green: 105, blue: 14).cgColor
        
        let layer = CAGradientLayer()
        layer.colors = [lightOrange, darkOrange]
        layer.locations = [0.0, 1.0]
        layer.frame = view.bounds
        view.layer.addSublayer(layer)
        return view
    }()
    
    let appLogo: UILabel = {
        let label = UILabel()
        label.text = "Lanysta"
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 40)
        return label
    }()
    
    let createAnAccountLabel: UILabel = {
        let label = UILabel()
        label.text = "Create An Account"
        label.textColor = UIColor.rgb(red: 17, green: 154, blue: 237)
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()
    
    let loginContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 4
        view.layer.shadowOffset = CGSize(width: 0, height: 3)
        view.layer.shadowRadius = 3
        view.layer.shadowOpacity = 0.5
        return view
    }()
    
    let emailTextField: UITextField = {
        let email = UITextField()
        email.placeholder = "Email"
        email.backgroundColor = UIColor(white: 0, alpha: 0.03)
        email.borderStyle = .roundedRect
        email.font = UIFont.systemFont(ofSize: 14)
        email.keyboardType = .emailAddress
        email.textContentType = .emailAddress
        email.autocapitalizationType = .none
        email.returnKeyType = .next
        
        email.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        
        return email
    }()
    
    let passwordTextField: UITextField = {
        let password = UITextField()
        password.placeholder = "Password"
        password.isSecureTextEntry = true
        password.backgroundColor = UIColor(white: 0, alpha: 0.03)
        password.borderStyle = .roundedRect
        password.font = UIFont.systemFont(ofSize: 14)
        if #available(iOS 12.0, *) {
            password.textContentType = .newPassword
        } else {
            // Fallback on earlier versions
            password.textContentType = .password
        }
        password.returnKeyType = .next
        
        password.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        
        return password
    }()
    
    let confirmPasswordTextField: UITextField = {
        let password = UITextField()
        password.placeholder = "Confirm Password"
        password.isSecureTextEntry = true
        password.backgroundColor = UIColor(white: 0, alpha: 0.03)
        password.borderStyle = .roundedRect
        password.font = UIFont.systemFont(ofSize: 14)
        if #available(iOS 12.0, *) {
            password.textContentType = .newPassword
        } else {
            // Fallback on earlier versions
            password.textContentType = .password
        }
        password.returnKeyType = .next
        
        password.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        
        return password
    }()
    
    let nextButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Next", for: .normal)
        button.backgroundColor = UIColor.rgb(red: 149, green: 204, blue: 244)
        button.layer.cornerRadius = 5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitleColor(.white, for: .normal)
        
        // Change selector to handleNext
        button.addTarget(self, action: #selector(handleSignUp), for: .touchUpInside)
        
        button.isEnabled = false
        
        return button
    }()
    
    @objc func handleSignUp() {
//        let nameAndNumberController = NameAndNumberController()
        navigationController?.popViewController(animated: true)
    }
    
    @objc func handleTextInputChange() {
        let isFormValid = emailTextField.text?.isEmpty == false && passwordTextField.text?.isEmpty == false && confirmPasswordTextField.text?.isEmpty == false
        
        if passwordTextField.text != confirmPasswordTextField.text {
            passwordTextField.backgroundColor = UIColor.red
            confirmPasswordTextField.backgroundColor = .red
        } else {
            passwordTextField.backgroundColor = UIColor(white: 0, alpha: 0.03)
            confirmPasswordTextField.backgroundColor = UIColor(white: 0, alpha: 0.03)
        }
        
        if isFormValid {
            nextButton.isEnabled = true
            nextButton.backgroundColor = UIColor.rgb(red: 17, green: 154, blue: 237)
        } else {
            nextButton.isEnabled = false
            nextButton.backgroundColor = UIColor.rgb(red: 149, green: 204, blue: 244)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setGradientBackground()
        
        view.addSubview(appLogo)
        appLogo.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: view.frame.height/14, paddingLeft: 0, paddingBotton: 0, paddingRight: 0, width: view.frame.width, height: 50)
        
        // Watching for touch out of keyboard to remove keyboard
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        
        setupInputFields()
    }
    
    fileprivate func setupInputFields() {
        
        view.addSubview(loginContainerView)
        loginContainerView.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, paddingTop: 20, paddingLeft: 16, paddingBotton: (view.frame.height/2)-80, paddingRight: 16, width: 0, height: 270)
        
        view.addSubview(createAnAccountLabel)
        createAnAccountLabel.anchor(top: loginContainerView.topAnchor, left: loginContainerView.leftAnchor, bottom: nil, right: loginContainerView.rightAnchor, paddingTop: 20, paddingLeft: 0, paddingBotton: 0, paddingRight: 0, width: 0, height: 50)
        
        //        let nameStackView = UIStackView(arrangedSubviews: [firstNameTextField, lastNameTextField])
        //        nameStackView.distribution = .fillEqually
        //        nameStackView.axis = .horizontal
        //        nameStackView.spacing = 10
        
        let stackView = UIStackView(arrangedSubviews: [emailTextField, passwordTextField, confirmPasswordTextField, nextButton])
        
        // Makes everything inside of the stackView array become distributed equally
        stackView.distribution = .fillEqually
        // Aligns the contents inside the stackView array to be aligned veritcally
        stackView.axis = .vertical
        // stackviews spacing inbetween elements of the array
        stackView.spacing = 10
        
        view.addSubview(stackView)
        
        // Auto Layout
        stackView.anchor(top: createAnAccountLabel.bottomAnchor, left: loginContainerView.leftAnchor, bottom: loginContainerView.bottomAnchor, right: loginContainerView.rightAnchor, paddingTop: 20, paddingLeft: 20, paddingBotton: 20, paddingRight: 20, width: 0, height: 0)
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
        confirmPasswordTextField.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        print("Keyboard has Appeared")
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        print("Keyboard is now gone")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailTextField:
            passwordTextField.becomeFirstResponder()
        case passwordTextField:
            confirmPasswordTextField.becomeFirstResponder()
        default:
            confirmPasswordTextField.resignFirstResponder()
            //            handleSignUp()
        }
        return true
    }
    
    func setGradientBackground() {
        let lightOrange = UIColor.rgb(red: 248, green: 148, blue: 6).cgColor
        let darkOrange = UIColor.rgb(red: 249, green: 105, blue: 14).cgColor
        
        let layer = CAGradientLayer()
        layer.colors = [lightOrange, darkOrange]
        layer.locations = [0.0, 1.0]
        layer.frame = self.view.bounds
        self.view.layer.addSublayer(layer)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}



















































